NAM [NuttX Application Manager]
===============================

This tool is a application manager for NuttX.

Even if NuttX has an "apps" repository, and nsh is the most used frontend to nuttx, 
apps are not a part of the NuttX RTOS.

Now that nuttx finds its application via a wildcard makefile rule, this tool 
makes it easy to integrate your own apps into the official NuttX's apps folder.

Apps can come from:
 - your local filesystem, 
 - git (checkout, submodule TODO)
 - svn (TODO)
 - a zip archive file (more archive types TODO).

the nam.py tool accepts the following modes:

nam list

This shows the list of the currently installed apps. This is just the list of the top-level directories that contains a Make.defs, except builtins and directories that start with a dot.

nam install [-git  <url>]
            [-svn  <url>] (TODO)
            [-zip  <path>] (TODO)
            [-dir  <path>] (TODO)
            [-link <path>] (TODO)

This installs the pointed application:
 - For git and svn, this checks out the application to a temp dir, checks the app manifest,
and renames the temp dir. 
 - For dir, this copies the directory contents.
 - For link, this creates a symlink to the directory.
 - for zip, this checks the manifest, then that all files go to their own directory
*or* their own include subdirectory, then extracts the archive in apps/.

nam remove <appname>

This removes the application from the apps/ dir. Confirmation is NOT requested atm.

Application Manifest
====================

The application manifest is a json encoded file with the following entries:
name (mandatory string) : the application name
version (mandatory string) : app version, not used yet.
git (optional string) : url for git, only used for app type at the moment.
svn (optional string) : url for subversion, idem.
have-include-subdir : TODO, links/install header files in the apps/include directory (used for "shared" libs)

The app manifest is stored in the app itself. So its possible, for example, to install an app from a zip file, while having a git url in the manifest; in that case <nam update> will update the application from git. At the moment, it will not work, since the unzipped contents will probably not have the structure of a git working directory. These situations must be detected and fixed.