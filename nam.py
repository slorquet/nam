#!/usr/bin/env python3
#
# nuttx package manager
# Copyright 2015 Sebastien Lorquet
# Author: Sebastien Lorquet <sebastien@lorquet.fr>
#
#
# supported commands
# list
# install git-co|svn
# remove
#
# this tool has to be run from the base directory of a nuttx environment.

# app types
# LEGACY    - Simple unmanged directory
# LOCALPATH - Symlink to a local directory outside of the apps/ tree.
# GITCO     - checked out GIT repository
# GITSM     - git submodule
# SVN       - Subversion checkout
# ARCHIVE   - Downloaded archive
#
# git and svn app types support the 'update' operation to download the latest sources.


###############################################################################

import sys
import os,shutil
import json

###############################################################################
# classes definitions
###############################################################################

class nuttxconfig:
  def __init__(self, cfgpath):
    conf = open(cfgpath, 'r')
    self.vars = dict()
    for l in conf:
      l = l.strip()
      if len(l)==0:   continue
      if l[0] == '#': continue
      name,sep,val = l.partition("=")
      #strip quotes
      if val.startswith("\"") and val.endswith("\""):
        val = val.strip("\"")
      self.vars[name] = val
    conf.close()

  def getvar(self,key):
    return self.vars[key]

###############################################################################

class nuttxapp:
  TYPE_LEGACY = 0
  TYPE_DIR    = 1
  TYPE_GITCO  = 2
  TYPE_GITSM  = 3
  TYPE_SVN    = 4

  def __init__(self, path):
    self.path = path
    try:
      metap = os.path.join(path,'nam.json')
      metaf = open(metap, 'r')
      meta  = json.load(metaf)
      self.type = self.TYPE_DIR
      self.name = meta['name']
      if 'git' in meta:
        self.type = self.TYPE_GITCO
        self.url = meta['git']
      elif 'svn' in meta:
        self.type = self.TYPE_SVN
        self.url = meta['svn']
      metaf.close()
    except IOError as e:
      self.type = self.TYPE_LEGACY
      self.name = os.path.basename(path)
    except Exception as e:
      print(e)
      self.type = None
      self.name = None

  def getpath(self):
    return self.path

  def getname(self):
    return self.name

  def gettype(self):
    return self.type

  def gettypestr(self):
    if self.type==self.TYPE_LEGACY:
      return 'legacy'
    elif self.type==self.TYPE_SVN:
      return 'svn-co'
    elif self.type==self.TYPE_GITCO:
      return 'git-co'
    elif self.type==self.TYPE_DIR:
      return 'directory'

  def update(self):
    print("TODO")

###############################################################################

class nuttx:
  def __init__(self, path):
    self.path = path

    #load the config file
    try:
      self.config = nuttxconfig(os.path.join(path,'.config'))
    except IOError:
      raise BaseException("Cannot open configuration file, is your nuttx tree configured?")
      raise e

    try:
      self.appdir = self.config.getvar('CONFIG_APPS_DIR')
    except KeyError:
      raise BaseException("CONFIG_APP_DIR is undefined, cannot continue!")

    print("app dir:",self.appdir)
    self.apps = dict()

    #load all apps
    for dirname in os.listdir(self.appdir):
      if dirname == 'builtin': continue
      subdir = os.path.normpath(os.path.join(self.appdir,dirname))
      if not os.path.isdir(subdir): continue
      if not os.path.exists(os.path.join(subdir,'Make.defs')): continue
      app = nuttxapp(subdir)
      self.apps[app.getname()] = app

  def foreachapp(self, cb):
    for key in self.apps:
      cb(self.apps[key])

  def install(self, type, element):
    if type=='git':
      tmpdir = os.path.join(self.appdir,".tmpgit")

      #clone into temp dir
      os.system('git clone --recursive '+element+' '+tmpdir)
      try:
        #read manifest
        temp = nuttxapp(tmpdir)
        appname = temp.getname()
        print()
        print("Installing:",appname)

        #check app dir does not exist yet
        appdir = os.path.join(self.appdir,appname)
        if os.path.exists(appdir):
          raise BasicException("The application already exists")

        #rename temp dir
        os.rename(tmpdir, appdir)
        print("Success.")

      except Exception as e:
        print("Installation failed: "+e)
        #delete temp dir
        shutil.rmtree(tmpdir)
        raise e

    elif type=='svn':
      tmpdir = os.path.join(self.appdir,".tmpsvn")

      #clone into temp dir
      os.system('svn checkout --force '+element+' '+tmpdir)
      try:
        #read manifest
        temp = nuttxapp(tmpdir)
        appname = temp.getname()
        print()
        print("Installing:",appname)

        #check app dir does not exist yet
        appdir = os.path.join(self.appdir,appname)
        if os.path.exists(appdir):
          raise BasicException("The application already exists")

        #rename temp dir
        os.rename(tmpdir, appdir)
        print("Success.")

      except Exception as e:
        print("Installation failed: "+e)
        #delete temp dir
        shutil.rmtree(tmpdir)
        raise e

    elif type=='zip':
      print('zip todo')

    else:
      raise BasicException("Unknown install mode!")

  def delete(self, appname):
    appdir = os.path.join(self.appdir,appname)
    if not os.path.exists(appdir):
      raise BaseException("The application does not exist")
    shutil.rmtree(appdir);
    print("Application",appname,"deleted.")

###############################################################################
# functions
###############################################################################

def display_cb(app):
  print(app.gettypestr() + " -> " + app.getname())

def update_cb(app):
  #legacy apps cannot be updated
  if app.gettype() == nuttxapp.TYPE_LEGACY: return 
  print("Updating:",app.getname())
  app.update()

def usage():
  print("list                     Display the list of currently available apps")
  print("update                   Bring updatable apps up to date")
  print("install -type <element>  Install an application")
  print("remove <appname>         Remove an application")
  sys.exit(1)

###############################################################################
# main program
###############################################################################

print("NuttX Application Manager")

if len(sys.argv) <= 1:
  usage()

action = sys.argv[1]

rtos = nuttx('.')

if action == "list":
  rtos.foreachapp(display_cb)

elif action == "update":
  rtos.foreachapp(update_cb)

elif action == "install":
  if len(sys.argv) != 4:
    usage()
  if sys.argv[2][0] != '-':
    usage()
  rtos.install(sys.argv[2][1:], sys.argv[3])

elif action == "remove":
  if len(sys.argv) != 3:
    usage()
  rtos.delete(sys.argv[2])

else:
  usage()

